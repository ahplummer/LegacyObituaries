FROM python:3
# Install dependencies
RUN apt-get update && apt-get install -y \
    python3-pip

# git the git.
WORKDIR /data
RUN git clone https://gitlab.com/ahplummer/LegacyObituaries.git /data/legacyobits
WORKDIR /data/legacyobits
RUN pip3 install -r requirements.txt
CMD cd /data/legacyobits && git pull && bash
